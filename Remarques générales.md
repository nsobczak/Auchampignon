# Remarques générales du projet bdd

## Généralités

On met des identifiants non signifiactifs par défaut: Identifiant Auto_increment.

- modèle de produit (clé = code EAN13 Varchar)
- produit unique 
- client

Le marketing choisit les produits qu'on veut. Les achats se fournissent en produits. La logistique les stocke ou les envoie dans les magasins. Les magasins vendent les produits et font la différence entre ce qui a été vendu et ce qui a été prévu.


## Marketing

Elabore l'offre de l'entreprise, gère la fidélisation des clients.
- gammes qui regroupent des modèles de produit
- 1 gamme a au moins une ligne (vache a lait)
- catégories de clients
- carte de fidélité de client
- prévisions de vente à comparer avec département des ventes


## Achats

Se fournit en modèles de produit auprès des fournisseurs.
- prix de vente conseillé
- modèles de produit
- produit unique
- gamme peut être une sous-gamme
- site de livraison 


## Logistique

Gère les stocks et les flux entre entrepôts et magasins.
- modèles de produit
- produit unique
- emplacement produit unique
- lieu géographique de stockage (adresse...)


## Ventes

Fixe des prix de vente.
- magasins concurrents ou non
- zone de challendise: potentiels clients
- modèle produits ont 3 prix à chaque fois
- produits vendus
- ticket de caisse lié à un magasin, points de fidélité si le client a une carte (ticket de caisse Auchampignon != ticket client qui sera généré à partir du ticket de caisse Auchampignon)
- clients
- prévisions de vente

