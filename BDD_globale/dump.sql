#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Modèle de produit
#------------------------------------------------------------

CREATE TABLE Modele_de_produit(
        code_EAN13                   Varchar (13) NOT NULL ,
        Libelle                      Varchar (25) NOT NULL ,
        Type_de_produit              Varchar (25) ,
        Points_de_fidelite_attribues Int ,
        Poids                        Float ,
        PRIMARY KEY (code_EAN13 )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Fournisseur
#------------------------------------------------------------

CREATE TABLE Fournisseur(
        SIRET          Varchar (25) NOT NULL ,
        Raison_sociale Varchar (25) ,
        PRIMARY KEY (SIRET )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Gamme
#------------------------------------------------------------

CREATE TABLE Gamme(
        Identifiant   int (11) Auto_increment  NOT NULL ,
        Libelle       Varchar (25) NOT NULL ,
        Identifiant_1 Int NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Commande
#------------------------------------------------------------

CREATE TABLE Commande(
        Identifiant                   int (11) Auto_increment  NOT NULL ,
        Date_de_commande              Date ,
        Date_de_livraison_imperative  Date ,
        Date_de_livraison_effective   Date ,
        Identifiant_Site_de_livraison Int NOT NULL ,
        SIRET                         Varchar (25) NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Site de livraison
#------------------------------------------------------------

CREATE TABLE Site_de_livraison(
        Identifiant                  int (11) Auto_increment  NOT NULL ,
        Type_de_site                 Varchar (25) ,
        Identifiant_Commande         Int NOT NULL ,
        Identifiant_Zone_de_stockage Int ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Tailles
#------------------------------------------------------------

CREATE TABLE Tailles(
        Identifiant int (11) Auto_increment  NOT NULL ,
        Libelle     Varchar (25) ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Coloris
#------------------------------------------------------------

CREATE TABLE Coloris(
        Identifiant int (11) Auto_increment  NOT NULL ,
        Libelle     Varchar (25) NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Produit unique
#------------------------------------------------------------

CREATE TABLE Produit_unique(
        Identifiant                                int (11) Auto_increment  NOT NULL ,
        Date_de_peremption                         Date ,
        code_EAN13                                 Varchar (13) NOT NULL ,
        Numero_de_ligne_de_commande                Int ,
        Identifiant_Commande                       Int NOT NULL ,
        Identifiant_Combinaison_tailles_coloris    Int NOT NULL ,
        Identifiant_Magasin                        Int NOT NULL ,
        Identifiant_Prix_de_vente                  Int NOT NULL ,
        Identifiant_Emplacement_stockage_theorique Int NOT NULL ,
        PRIMARY KEY (Identifiant ,code_EAN13 )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Combinaison tailles coloris
#------------------------------------------------------------

CREATE TABLE Combinaison_tailles_coloris(
        Identifiant int (11) Auto_increment  NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Tarif HT
#------------------------------------------------------------

CREATE TABLE Tarif_HT(
        Identifiant                        int (11) Auto_increment  NOT NULL ,
        Tarif                              DECIMAL (15,3)  NOT NULL ,
        Date_de_debut_de_validite_du_tarif Date ,
        Date_de_fin_de_validite_du_tarif   Date ,
        Quantite_du_produit                Int NOT NULL ,
        SIRET                              Varchar (25) NOT NULL ,
        code_EAN13                         Varchar (13) ,
        PRIMARY KEY (Identifiant ,SIRET )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Ligne de commande
#------------------------------------------------------------

CREATE TABLE Ligne_de_commande(
        Numero_de_ligne_de_commande    Int NOT NULL ,
        Quantite_de_produits_commandes Int ,
        Identifiant                    Int NOT NULL ,
        code_EAN13                     Varchar (13) ,
        PRIMARY KEY (Numero_de_ligne_de_commande ,Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Magasin
#------------------------------------------------------------

CREATE TABLE Magasin(
        Identifiant                  int (11) Auto_increment  NOT NULL ,
        Concurrent                   Bool ,
        Identifiant_Zone_chalandise  Int NOT NULL ,
        Identifiant_Adresse          Int NOT NULL ,
        Identifiant_Zone_de_stockage Int ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Zone chalandise
#------------------------------------------------------------

CREATE TABLE Zone_chalandise(
        Identifiant     int (11) Auto_increment  NOT NULL ,
        Caracteristique Varchar (25) ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Ticket de caisse
#------------------------------------------------------------

CREATE TABLE Ticket_de_caisse(
        Identifiant         int (11) Auto_increment  NOT NULL ,
        Date_de_vente       Date ,
        TVA                 Float ,
        Identifiant_Client  Int NOT NULL ,
        Identifiant_Magasin Int NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Client
#------------------------------------------------------------

CREATE TABLE Client(
        Identifiant         int (11) Auto_increment  NOT NULL ,
        Nom                 Varchar (25) ,
        Prenom              Varchar (25) ,
        email               Varchar (25) ,
        Fidelise            Bool ,
        Points_de_fidelite  Int ,
        Identifiant_Adresse Int NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Prix de vente
#------------------------------------------------------------

CREATE TABLE Prix_de_vente(
        Identifiant         int (11) Auto_increment  NOT NULL ,
        Prix                DECIMAL (15,3)  ,
        Date_debut_validite Date ,
        Date_fin_validite   Date ,
        Identifiant_Magasin Int NOT NULL ,
        code_EAN13          Varchar (13) ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Ligne de modèle de produit
#------------------------------------------------------------

CREATE TABLE Ligne_de_modele_de_produit(
        Identifiant int (11) Auto_increment  NOT NULL ,
        Libelle     Varchar (25) ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Catégorie client
#------------------------------------------------------------

CREATE TABLE Categorie_client(
        Identifiant int (11) Auto_increment  NOT NULL ,
        Libelle     Varchar (25) NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Adresse
#------------------------------------------------------------

CREATE TABLE Adresse(
        Identifiant int (11) Auto_increment  NOT NULL ,
        Numero      Varchar (25) ,
        Voie        Varchar (25) ,
        Code_postal Varchar (25) ,
        Ville       Varchar (25) ,
        Pays        Varchar (25) ,
        Remarques   Longtext ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Ligne de ticket de caisse
#------------------------------------------------------------

CREATE TABLE Ligne_de_ticket_de_caisse(
        Identifiant                  int (11) Auto_increment  NOT NULL ,
        Identifiant_Ticket_de_caisse Int NOT NULL ,
        Identifiant_Produit_unique   Int ,
        code_EAN13                   Varchar (13) ,
        PRIMARY KEY (Identifiant ,Identifiant_Ticket_de_caisse )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Prévision de vente
#------------------------------------------------------------

CREATE TABLE Prevision_de_vente(
        Identifiant         int (11) Auto_increment  NOT NULL ,
        Date_prevision      Date ,
        Quantite            Int ,
        Identifiant_Magasin Int NOT NULL ,
        code_EAN13          Varchar (13) ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Previsions
#------------------------------------------------------------

CREATE TABLE Previsions(
        id         int (11) Auto_increment  NOT NULL ,
        Date_debut Date ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Prevision par mois
#------------------------------------------------------------

CREATE TABLE Prevision_par_mois(
        id            int (11) Auto_increment  NOT NULL ,
        Mois          Int ,
        Quantite      Int ,
        id_Previsions Int ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Démarque
#------------------------------------------------------------

CREATE TABLE Demarque(
        ID_demarque                                int (11) Auto_increment  NOT NULL ,
        Vol                                        Bool ,
        Casse                                      Bool ,
        Identifiant                                Int ,
        code_EAN13                                 Varchar (13) ,
        Identifiant_Emplacement_stockage_theorique Int NOT NULL ,
        PRIMARY KEY (ID_demarque )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Emplacement stockage théorique
#------------------------------------------------------------

CREATE TABLE Emplacement_stockage_theorique(
        Identifiant                  int (11) Auto_increment  NOT NULL ,
        Type_stockage                Varchar (25) NOT NULL ,
        Capacite                     BigInt NOT NULL ,
        Taille                       Int ,
        Identifiant_Zone_de_stockage Int ,
        Identifiant_Magasin          Int NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Zone de stockage
#------------------------------------------------------------

CREATE TABLE Zone_de_stockage(
        Identifiant                        int (11) Auto_increment  NOT NULL ,
        Type_de_zone_de_stockage           Varchar (25) ,
        Capacite_stockage_ambiant_max      Int ,
        Capacite_stockage_refrigere_max    Int ,
        Capacite_stockage_congele_max      Int ,
        Capacite_stockage_ambiant_actuel   Int ,
        Capacite_stockage_refrigere_actuel Int ,
        Capacite_stockage_congele_actuel   Int ,
        Identifiant_Site_de_livraison      Int NOT NULL ,
        Identifiant_Adresse                Int NOT NULL ,
        Identifiant_Magasin                Int NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Vache à lait compose
#------------------------------------------------------------

CREATE TABLE Vache_a_lait_compose(
        code_EAN13  Varchar (13) NOT NULL ,
        Identifiant Int NOT NULL ,
        PRIMARY KEY (code_EAN13 ,Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Tailles possibles
#------------------------------------------------------------

CREATE TABLE Tailles_possibles(
        Identifiant                             Int NOT NULL ,
        Identifiant_Combinaison_tailles_coloris Int NOT NULL ,
        PRIMARY KEY (Identifiant ,Identifiant_Combinaison_tailles_coloris )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Coloris possibles
#------------------------------------------------------------

CREATE TABLE Coloris_possibles(
        Identifiant                             Int NOT NULL ,
        Identifiant_Combinaison_tailles_coloris Int NOT NULL ,
        PRIMARY KEY (Identifiant ,Identifiant_Combinaison_tailles_coloris )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Fournir
#------------------------------------------------------------

CREATE TABLE Fournir(
        Code_fournisseur Varchar (25) NOT NULL ,
        code_EAN13       Varchar (13) NOT NULL ,
        SIRET            Varchar (25) NOT NULL ,
        PRIMARY KEY (code_EAN13 ,SIRET )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Combinaisons disponibles
#------------------------------------------------------------

CREATE TABLE Combinaisons_disponibles(
        Identifiant Int NOT NULL ,
        code_EAN13  Varchar (13) NOT NULL ,
        PRIMARY KEY (Identifiant ,code_EAN13 )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Appartenir gamme
#------------------------------------------------------------

CREATE TABLE Appartenir_gamme(
        Identifiant                            Int NOT NULL ,
        Identifiant_Ligne_de_modele_de_produit Int NOT NULL ,
        PRIMARY KEY (Identifiant ,Identifiant_Ligne_de_modele_de_produit )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Appartient
#------------------------------------------------------------

CREATE TABLE Appartient(
        Identifiant                  Int NOT NULL ,
        Identifiant_Categorie_client Int NOT NULL ,
        PRIMARY KEY (Identifiant ,Identifiant_Categorie_client )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Concerne
#------------------------------------------------------------

CREATE TABLE Concerne(
        code_EAN13  Varchar (13) NOT NULL ,
        Identifiant Int NOT NULL ,
        PRIMARY KEY (code_EAN13 ,Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Porte sur modèle
#------------------------------------------------------------

CREATE TABLE Porte_sur_modele(
        id         Int NOT NULL ,
        code_EAN13 Varchar (13) NOT NULL ,
        PRIMARY KEY (id ,code_EAN13 )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Leader compose
#------------------------------------------------------------

CREATE TABLE Leader_compose(
        code_EAN13  Varchar (13) NOT NULL ,
        Identifiant Int NOT NULL ,
        PRIMARY KEY (code_EAN13 ,Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Accroche compose
#------------------------------------------------------------

CREATE TABLE Accroche_compose(
        Identifiant Int NOT NULL ,
        code_EAN13  Varchar (13) NOT NULL ,
        PRIMARY KEY (Identifiant ,code_EAN13 )
)ENGINE=InnoDB;

ALTER TABLE Gamme ADD CONSTRAINT FK_Gamme_Identifiant_1 FOREIGN KEY (Identifiant_1) REFERENCES Gamme(Identifiant);
ALTER TABLE Commande ADD CONSTRAINT FK_Commande_Identifiant_Site_de_livraison FOREIGN KEY (Identifiant_Site_de_livraison) REFERENCES Site_de_livraison(Identifiant);
ALTER TABLE Commande ADD CONSTRAINT FK_Commande_SIRET FOREIGN KEY (SIRET) REFERENCES Fournisseur(SIRET);
ALTER TABLE Site_de_livraison ADD CONSTRAINT FK_Site_de_livraison_Identifiant_Commande FOREIGN KEY (Identifiant_Commande) REFERENCES Commande(Identifiant);
ALTER TABLE Site_de_livraison ADD CONSTRAINT FK_Site_de_livraison_Identifiant_Zone_de_stockage FOREIGN KEY (Identifiant_Zone_de_stockage) REFERENCES Zone_de_stockage(Identifiant);
ALTER TABLE Produit_unique ADD CONSTRAINT FK_Produit_unique_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Produit_unique ADD CONSTRAINT FK_Produit_unique_Numero_de_ligne_de_commande FOREIGN KEY (Numero_de_ligne_de_commande) REFERENCES Ligne_de_commande(Numero_de_ligne_de_commande);
ALTER TABLE Produit_unique ADD CONSTRAINT FK_Produit_unique_Identifiant_Commande FOREIGN KEY (Identifiant_Commande) REFERENCES Commande(Identifiant);
ALTER TABLE Produit_unique ADD CONSTRAINT FK_Produit_unique_Identifiant_Combinaison_tailles_coloris FOREIGN KEY (Identifiant_Combinaison_tailles_coloris) REFERENCES Combinaison_tailles_coloris(Identifiant);
ALTER TABLE Produit_unique ADD CONSTRAINT FK_Produit_unique_Identifiant_Magasin FOREIGN KEY (Identifiant_Magasin) REFERENCES Magasin(Identifiant);
ALTER TABLE Produit_unique ADD CONSTRAINT FK_Produit_unique_Identifiant_Prix_de_vente FOREIGN KEY (Identifiant_Prix_de_vente) REFERENCES Prix_de_vente(Identifiant);
ALTER TABLE Produit_unique ADD CONSTRAINT FK_Produit_unique_Identifiant_Emplacement_stockage_theorique FOREIGN KEY (Identifiant_Emplacement_stockage_theorique) REFERENCES Emplacement_stockage_theorique(Identifiant);
ALTER TABLE Tarif_HT ADD CONSTRAINT FK_Tarif_HT_SIRET FOREIGN KEY (SIRET) REFERENCES Fournisseur(SIRET);
ALTER TABLE Tarif_HT ADD CONSTRAINT FK_Tarif_HT_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Ligne_de_commande ADD CONSTRAINT FK_Ligne_de_commande_Identifiant FOREIGN KEY (Identifiant) REFERENCES Commande(Identifiant);
ALTER TABLE Ligne_de_commande ADD CONSTRAINT FK_Ligne_de_commande_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Magasin ADD CONSTRAINT FK_Magasin_Identifiant_Zone_chalandise FOREIGN KEY (Identifiant_Zone_chalandise) REFERENCES Zone_chalandise(Identifiant);
ALTER TABLE Magasin ADD CONSTRAINT FK_Magasin_Identifiant_Adresse FOREIGN KEY (Identifiant_Adresse) REFERENCES Adresse(Identifiant);
ALTER TABLE Magasin ADD CONSTRAINT FK_Magasin_Identifiant_Zone_de_stockage FOREIGN KEY (Identifiant_Zone_de_stockage) REFERENCES Zone_de_stockage(Identifiant);
ALTER TABLE Ticket_de_caisse ADD CONSTRAINT FK_Ticket_de_caisse_Identifiant_Client FOREIGN KEY (Identifiant_Client) REFERENCES Client(Identifiant);
ALTER TABLE Ticket_de_caisse ADD CONSTRAINT FK_Ticket_de_caisse_Identifiant_Magasin FOREIGN KEY (Identifiant_Magasin) REFERENCES Magasin(Identifiant);
ALTER TABLE Client ADD CONSTRAINT FK_Client_Identifiant_Adresse FOREIGN KEY (Identifiant_Adresse) REFERENCES Adresse(Identifiant);
ALTER TABLE Prix_de_vente ADD CONSTRAINT FK_Prix_de_vente_Identifiant_Magasin FOREIGN KEY (Identifiant_Magasin) REFERENCES Magasin(Identifiant);
ALTER TABLE Prix_de_vente ADD CONSTRAINT FK_Prix_de_vente_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Ligne_de_ticket_de_caisse ADD CONSTRAINT FK_Ligne_de_ticket_de_caisse_Identifiant_Ticket_de_caisse FOREIGN KEY (Identifiant_Ticket_de_caisse) REFERENCES Ticket_de_caisse(Identifiant);
ALTER TABLE Ligne_de_ticket_de_caisse ADD CONSTRAINT FK_Ligne_de_ticket_de_caisse_Identifiant_Produit_unique FOREIGN KEY (Identifiant_Produit_unique) REFERENCES Produit_unique(Identifiant);
ALTER TABLE Ligne_de_ticket_de_caisse ADD CONSTRAINT FK_Ligne_de_ticket_de_caisse_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Prevision_de_vente ADD CONSTRAINT FK_Prevision_de_vente_Identifiant_Magasin FOREIGN KEY (Identifiant_Magasin) REFERENCES Magasin(Identifiant);
ALTER TABLE Prevision_de_vente ADD CONSTRAINT FK_Prevision_de_vente_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Prevision_par_mois ADD CONSTRAINT FK_Prevision_par_mois_id_Previsions FOREIGN KEY (id_Previsions) REFERENCES Previsions(id);
ALTER TABLE Demarque ADD CONSTRAINT FK_Demarque_Identifiant FOREIGN KEY (Identifiant) REFERENCES Produit_unique(Identifiant);
ALTER TABLE Demarque ADD CONSTRAINT FK_Demarque_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Demarque ADD CONSTRAINT FK_Demarque_Identifiant_Emplacement_stockage_theorique FOREIGN KEY (Identifiant_Emplacement_stockage_theorique) REFERENCES Emplacement_stockage_theorique(Identifiant);
ALTER TABLE Emplacement_stockage_theorique ADD CONSTRAINT FK_Emplacement_stockage_theorique_Identifiant_Zone_de_stockage FOREIGN KEY (Identifiant_Zone_de_stockage) REFERENCES Zone_de_stockage(Identifiant);
ALTER TABLE Emplacement_stockage_theorique ADD CONSTRAINT FK_Emplacement_stockage_theorique_Identifiant_Magasin FOREIGN KEY (Identifiant_Magasin) REFERENCES Magasin(Identifiant);
ALTER TABLE Zone_de_stockage ADD CONSTRAINT FK_Zone_de_stockage_Identifiant_Site_de_livraison FOREIGN KEY (Identifiant_Site_de_livraison) REFERENCES Site_de_livraison(Identifiant);
ALTER TABLE Zone_de_stockage ADD CONSTRAINT FK_Zone_de_stockage_Identifiant_Adresse FOREIGN KEY (Identifiant_Adresse) REFERENCES Adresse(Identifiant);
ALTER TABLE Zone_de_stockage ADD CONSTRAINT FK_Zone_de_stockage_Identifiant_Magasin FOREIGN KEY (Identifiant_Magasin) REFERENCES Magasin(Identifiant);
ALTER TABLE Vache_a_lait_compose ADD CONSTRAINT FK_Vache_a_lait_compose_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Vache_a_lait_compose ADD CONSTRAINT FK_Vache_a_lait_compose_Identifiant FOREIGN KEY (Identifiant) REFERENCES Ligne_de_modele_de_produit(Identifiant);
ALTER TABLE Tailles_possibles ADD CONSTRAINT FK_Tailles_possibles_Identifiant FOREIGN KEY (Identifiant) REFERENCES Tailles(Identifiant);
ALTER TABLE Tailles_possibles ADD CONSTRAINT FK_Tailles_possibles_Identifiant_Combinaison_tailles_coloris FOREIGN KEY (Identifiant_Combinaison_tailles_coloris) REFERENCES Combinaison_tailles_coloris(Identifiant);
ALTER TABLE Coloris_possibles ADD CONSTRAINT FK_Coloris_possibles_Identifiant FOREIGN KEY (Identifiant) REFERENCES Coloris(Identifiant);
ALTER TABLE Coloris_possibles ADD CONSTRAINT FK_Coloris_possibles_Identifiant_Combinaison_tailles_coloris FOREIGN KEY (Identifiant_Combinaison_tailles_coloris) REFERENCES Combinaison_tailles_coloris(Identifiant);
ALTER TABLE Fournir ADD CONSTRAINT FK_Fournir_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Fournir ADD CONSTRAINT FK_Fournir_SIRET FOREIGN KEY (SIRET) REFERENCES Fournisseur(SIRET);
ALTER TABLE Combinaisons_disponibles ADD CONSTRAINT FK_Combinaisons_disponibles_Identifiant FOREIGN KEY (Identifiant) REFERENCES Combinaison_tailles_coloris(Identifiant);
ALTER TABLE Combinaisons_disponibles ADD CONSTRAINT FK_Combinaisons_disponibles_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Appartenir_gamme ADD CONSTRAINT FK_Appartenir_gamme_Identifiant FOREIGN KEY (Identifiant) REFERENCES Gamme(Identifiant);
ALTER TABLE Appartenir_gamme ADD CONSTRAINT FK_Appartenir_gamme_Identifiant_Ligne_de_modele_de_produit FOREIGN KEY (Identifiant_Ligne_de_modele_de_produit) REFERENCES Ligne_de_modele_de_produit(Identifiant);
ALTER TABLE Appartient ADD CONSTRAINT FK_Appartient_Identifiant FOREIGN KEY (Identifiant) REFERENCES Client(Identifiant);
ALTER TABLE Appartient ADD CONSTRAINT FK_Appartient_Identifiant_Categorie_client FOREIGN KEY (Identifiant_Categorie_client) REFERENCES Categorie_client(Identifiant);
ALTER TABLE Concerne ADD CONSTRAINT FK_Concerne_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Concerne ADD CONSTRAINT FK_Concerne_Identifiant FOREIGN KEY (Identifiant) REFERENCES Categorie_client(Identifiant);
ALTER TABLE Porte_sur_modele ADD CONSTRAINT FK_Porte_sur_modele_id FOREIGN KEY (id) REFERENCES Prevision_par_mois(id);
ALTER TABLE Porte_sur_modele ADD CONSTRAINT FK_Porte_sur_modele_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Leader_compose ADD CONSTRAINT FK_Leader_compose_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Leader_compose ADD CONSTRAINT FK_Leader_compose_Identifiant FOREIGN KEY (Identifiant) REFERENCES Ligne_de_modele_de_produit(Identifiant);
ALTER TABLE Accroche_compose ADD CONSTRAINT FK_Accroche_compose_Identifiant FOREIGN KEY (Identifiant) REFERENCES Ligne_de_modele_de_produit(Identifiant);
ALTER TABLE Accroche_compose ADD CONSTRAINT FK_Accroche_compose_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);

