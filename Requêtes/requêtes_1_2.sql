# author : Thibault Fruchart, Gustave Toison, Pierre Trouche-Brugger
#_______________________________________________________________________________________________________________________________________________________________________

-- Liste de produit périmé dans moins de une semaine par stockage requête 1
SELECT zone_de_stockage.Identifiant as ID_stockage, modele_de_produit.Type_de_produit,produit_unique.Identifiant, produit_unique.Date_de_peremption
FROM zone_de_stockage, modele_de_produit,produit_unique, emplacement_stockage_theorique
WHERE DATEDIFF(produit_unique.Date_de_peremption, CURDATE()) < 7 AND emplacement_stockage_theorique.Identifiant_Zone_de_stockage = zone_de_stockage.Identifiant AND emplacement_stockage_theorique.Identifiant = produit_unique.Identifiant_Emplacement_stockage_theorique
ORDER BY modele_de_produit.Type_de_produit


