# author : Timothée de Montety & Julien Vermeil
#_______________________________________________________________________________________________________________________________________________________________________

# 3) Etablissez la liste des produits achetés par client et par type de produit (client, type
#produit, quantité moyenne par achat, quantité totale, prix moyen par achat, prix
#total) pour une période donnée (année) 

#liste de tous les modèles produits achetés
SELECT ligne_de_ticket_de_caisse.code_EAN13 
FROM ligne_de_ticket_de_caisse;

#listes des clients qui ont acheté
Select ticket_de_caisse.Identifiant_Client 
from ticket_de_caisse;

#Jointure entre ticket de caisse et ligne de ticket de caisse
SELECT ticket_de_caisse.Identifiant_Client, ligne_de_ticket_de_caisse.code_EAN13
FROM ticket_de_caisse
INNER JOIN ligne_de_ticket_de_caisse
WHERE ticket_de_caisse.Identifiant = ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse;


# Compte le nombre de ticket de caisse par client
select ticket_de_caisse.Identifiant_Client, count(ticket_de_caisse.Identifiant)
from ticket_de_caisse
group by ticket_de_caisse.Identifiant_Client;

# comptage des produits par type
select * from (
select Modele_de_produit.Type_de_produit, COUNT(Ligne_de_ticket_de_caisse.Identifiant) as Quantité_de_produits_vendus
from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
group by Modele_de_produit.Type_de_produit
)as Nombre_produit_par_type;

## comptage des produits par type par client par année
select * from(
select Ticket_de_caisse.Date_de_vente, Ticket_de_caisse.Identifiant_Client, 
		Nombre_produit_par_type.Type_de_produit, COUNT(Nombre_produit_par_type.Identifiant) as Quantité_de_produits_vendus 
		from (
			select Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse
			from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
			group by Modele_de_produit.Type_de_produit
		)as Nombre_produit_par_type
		inner join Ticket_de_caisse
		on Nombre_produit_par_type.Identifiant_Ticket_de_caisse = Ticket_de_caisse.Identifiant
		where Ticket_de_caisse.Date_de_vente like '%2016%'
		group by Ticket_de_caisse.Identifiant_Client
) as Nombre_produit_par_type_par_client_par_annee;


# requête finale : liste des produits achetés par client (client, type produit, quantité, quantité moyenne par achat) pour une période donnée (année)
select Nombre_produit_par_type_par_client_par_annee.Date_de_vente, Nombre_produit_par_type_par_client_par_annee.Identifiant_Client, 
	Nombre_produit_par_type_par_client_par_annee.Type_de_produit, Nombre_produit_par_type_par_client_par_annee.Quantité_de_produits_vendus,
	Quantite_moyenne_type_produit_par_achat_par_client_par_annee.Moyenne_produits_achetés_par_achat
		
from (
	select Ticket_de_caisse.Date_de_vente, Ticket_de_caisse.Identifiant_Client, 
			Nombre_produit_par_type.Type_de_produit, COUNT(Nombre_produit_par_type.Identifiant) as Quantité_de_produits_vendus 
			from (
				select Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse
				from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
				group by Modele_de_produit.Type_de_produit
			)as Nombre_produit_par_type
			inner join Ticket_de_caisse
			on Nombre_produit_par_type.Identifiant_Ticket_de_caisse = Ticket_de_caisse.Identifiant
			where Ticket_de_caisse.Date_de_vente like '%2016%'
			group by Ticket_de_caisse.Identifiant_Client
	) as Nombre_produit_par_type_par_client_par_annee
inner join (
	select Quantite_type_produit_par_achat_par_client_par_annee.Date_de_vente, Quantite_type_produit_par_achat_par_client_par_annee.Identifiant_Client,
				Quantite_type_produit_par_achat_par_client_par_annee.Type_de_produit, 
				avg(Quantite_type_produit_par_achat_par_client_par_annee.Nombre_de_produits_achétés_par_achat) as Moyenne_produits_achetés_par_achat
	from (
		select Ticket_de_caisse.Date_de_vente, Ticket_de_caisse.Identifiant_Client,
				Quantite_moyenne_type_produit_par_achat.Type_de_produit, Quantite_moyenne_type_produit_par_achat.Identifiant_Ticket_de_caisse,
				Quantite_moyenne_type_produit_par_achat.Nombre_de_produits_achétés_par_achat
		from (select Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse, count(Ligne_de_ticket_de_caisse.Identifiant) as Nombre_de_produits_achétés_par_achat
					from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
					group by Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse
				)as Quantite_moyenne_type_produit_par_achat
				inner join Ticket_de_caisse
				on Quantite_moyenne_type_produit_par_achat.Identifiant_Ticket_de_caisse = Ticket_de_caisse.Identifiant
				where Ticket_de_caisse.Date_de_vente like '%2016%'
				group by Ticket_de_caisse.Identifiant_Client
		) as Quantite_type_produit_par_achat_par_client_par_annee
	group by Quantite_type_produit_par_achat_par_client_par_annee.Type_de_produit
	) as Quantite_moyenne_type_produit_par_achat_par_client_par_annee
on (Nombre_produit_par_type_par_client_par_annee.Date_de_vente = Quantite_moyenne_type_produit_par_achat_par_client_par_annee.Date_de_vente) 
	and (Nombre_produit_par_type_par_client_par_annee.Identifiant_Client = Quantite_moyenne_type_produit_par_achat_par_client_par_annee.Identifiant_Client) 
	and (Nombre_produit_par_type_par_client_par_annee.Type_de_produit = Quantite_moyenne_type_produit_par_achat_par_client_par_annee.Type_de_produit) 

order by Nombre_produit_par_type_par_client_par_annee.Identifiant_Client, Nombre_produit_par_type_par_client_par_annee.Type_de_produit, 
		Nombre_produit_par_type_par_client_par_annee.Date_de_vente;



#_______________________________________________________________________________________________________________________________________________________________________
# 4) Etablissez pour chaque magasin et entrepôt la liste de tous les produits non
# disponibles (type produit) 

# liste théorique de tous les types de produit possibles
SELECT modele_de_produit.Type_de_produit 
FROM modele_de_produit;


# liste théorique de toutes les zones de stockage possibles  
SELECT emplacement_stockage_theorique.Identifiant_Zone_de_stockage
FROM emplacement_stockage_theorique;


# liste théorique de tous les types de produit possibles par toutes les zones de stockage possibles  
select * from(
SELECT emplacement_stockage_theorique.Identifiant_Zone_de_stockage,
		modele_de_produit.Type_de_produit
FROM modele_de_produit
cross join emplacement_stockage_theorique
)as combinaisons_possibles_type_produit_zone_stockage;


# liste des produits uniques par zone de stockage
select * from(
select emplacement_stockage_theorique.Identifiant_Zone_de_stockage, modele_de_produit.Type_de_produit
from produit_unique
inner join emplacement_stockage_theorique 
	on produit_unique.Identifiant_Emplacement_stockage_theorique = emplacement_stockage_theorique.Identifiant
inner join modele_de_produit
	on produit_unique.code_EAN13 = modele_de_produit.code_EAN13
where emplacement_stockage_theorique.Identifiant_Magasin is null
group by emplacement_stockage_theorique.Identifiant_Zone_de_stockage,
		modele_de_produit.Type_de_produit
)as type_produit_presents_par_zone;
	
        
        
#requete finale
SELECT combinaisons_possibles_type_produit_zone_stockage.Identifiant_Zone_de_stockage,
		combinaisons_possibles_type_produit_zone_stockage.Type_de_produit

from (SELECT emplacement_stockage_theorique.Identifiant_Zone_de_stockage,
			modele_de_produit.Type_de_produit
	FROM modele_de_produit
	cross join emplacement_stockage_theorique
	)as combinaisons_possibles_type_produit_zone_stockage

left join (select emplacement_stockage_theorique.Identifiant_Zone_de_stockage, modele_de_produit.Type_de_produit
			from produit_unique
			inner join emplacement_stockage_theorique 
				on produit_unique.Identifiant_Emplacement_stockage_theorique = emplacement_stockage_theorique.Identifiant
			inner join modele_de_produit
				on produit_unique.code_EAN13 = modele_de_produit.code_EAN13
			where emplacement_stockage_theorique.Identifiant_Magasin is null
			group by emplacement_stockage_theorique.Identifiant_Zone_de_stockage,
					modele_de_produit.Type_de_produit
			)as type_produit_presents_par_zone

	on combinaisons_possibles_type_produit_zone_stockage.Identifiant_Zone_de_stockage = type_produit_presents_par_zone.Identifiant_Zone_de_stockage
	and combinaisons_possibles_type_produit_zone_stockage.Type_de_produit = type_produit_presents_par_zone.Type_de_produit

where type_produit_presents_par_zone.Type_de_produit is null
;
