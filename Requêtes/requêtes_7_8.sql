# authors : Vincent Reaynaert & Nicolas Sobczak
#__________________________________________________________________________________________________________________________________________________________________
# === 7 ===

# nombre de produit par type de produit par commande (=achats) et année
select * from(
select Commande.Date_de_commande, Commande.Identifiant as Identifiant_commande,
		Modele_de_produit.Type_de_produit, sum(Ligne_de_commande.Quantite_de_produits_commandes) as nombre_type_produit_commandes
from Modele_de_produit 
inner join Ligne_de_commande on Ligne_de_commande.code_EAN13 = Modele_de_produit.code_EAN13
inner join Commande on Ligne_de_commande.Identifiant = Commande.Identifiant
where Commande.Date_de_commande like '%2016%'
group by Identifiant_commande, Modele_de_produit.Type_de_produit
)as type_produit_par_commande_par_annee;


# commandes par magasins
select * from(
select Zone_de_stockage.Identifiant_Magasin, Site_de_livraison.Identifiant_commande
from Site_de_livraison
inner join Zone_de_stockage
on Site_de_livraison.Identifiant_Zone_de_stockage = Zone_de_stockage.Identifiant
where Zone_de_stockage.Identifiant_Magasin is not null
group by Zone_de_stockage.Identifiant_Magasin
)as commandes_par_magasins;


select type_produit_par_commande_par_annee.Date_de_commande, commandes_par_magasins.Identifiant_Magasin, 
		type_produit_par_commande_par_annee.Type_de_produit, 
		sum(type_produit_par_commande_par_annee.nombre_type_produit_commandes) as Nombre_de_produits_achetés_par_magasin_par_type_de_produit,
		avg(type_produit_par_commande_par_annee.nombre_type_produit_commandes) as Nombre_de_produits_moyen_achetés_par_magasin_par_type_de_produit
from (select Commande.Date_de_commande, Commande.Identifiant as Identifiant_commande,
				Modele_de_produit.Type_de_produit, sum(Ligne_de_commande.Quantite_de_produits_commandes) as nombre_type_produit_commandes
		from Modele_de_produit 
		inner join Ligne_de_commande on Ligne_de_commande.code_EAN13 = Modele_de_produit.code_EAN13
		inner join Commande on Ligne_de_commande.Identifiant = Commande.Identifiant
		where Commande.Date_de_commande like '%2016%'
		group by Identifiant_commande, Modele_de_produit.Type_de_produit
		)as type_produit_par_commande_par_annee
inner join (select Zone_de_stockage.Identifiant_Magasin, Site_de_livraison.Identifiant_commande
		from Site_de_livraison
		inner join Zone_de_stockage
		on Site_de_livraison.Identifiant_Zone_de_stockage = Zone_de_stockage.Identifiant
		where Zone_de_stockage.Identifiant_Magasin is not null
		group by Zone_de_stockage.Identifiant_Magasin
		)as commandes_par_magasins
on type_produit_par_commande_par_annee.Identifiant_commande = commandes_par_magasins.Identifiant_commande
group by commandes_par_magasins.Identifiant_Magasin
order by commandes_par_magasins.Identifiant_Magasin, type_produit_par_commande_par_annee.Type_de_produit;



#__________________________________________________________________________________________________________________________________________________________________
# === 8 ===

#---Prevision---

#Par magasin pour le mois précédent et pour chaque type de produit, la quantité prévisionnelle
SELECT
	Prevision_de_vente.Identifiant_Magasin,
	Modele_de_produit.Type_de_produit,
	Prevision_de_vente.Quantite
	
FROM Prevision_de_vente
	INNER JOIN Modele_de_produit 	ON (Modele_de_produit.code_EAN13 = Prevision_de_vente.code_EAN13)

WHERE Prevision_de_vente.Date_prevision 
	LIKE '__-02'
	
	#si on veut avant le mois precedent il faut passer par la date actuelle comme si dessous au lieu de placer le mois en dur
	#BETWEEN LAST_DAY(TRUNC(ADD_MONTHS(SYSDATE, -2))) + 1
	#AND LAST_DAY(TRUNC(ADD_MONTHS(SYSDATE, -1)))

ORDER BY  Prevision_de_vente.Identifiant_Magasin;


#---Vente---

#Par magasin pour le mois précédent et pour chaque type de produit, la quantité de ventes réalisée

SELECT COUNT(quantite_modele_par_ticket.Un_Produit_Vendu) AS Quantite_vendue,
		Modele_de_produit.Type_de_produit,
		Ticket_de_caisse.Identifiant_Magasin
    
FROM (SELECT Ligne_de_ticket_de_caisse.Identifiant  				AS Un_Produit_Vendu,
			Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse 	AS Identifiant_Ticket,
			Ligne_de_ticket_de_caisse.code_EAN13                   	AS EAN13
		FROM Ligne_de_ticket_de_caisse
		) AS quantite_modele_par_ticket

INNER JOIN Ticket_de_caisse 	ON (Ticket_de_caisse.Identifiant = quantite_modele_par_ticket.Identifiant_Ticket)
INNER JOIN Modele_de_produit 	ON (Modele_de_produit.code_EAN13 = quantite_modele_par_ticket.EAN13)
 
WHERE Ticket_de_caisse.Date_de_vente LIKE '__-02' 

GROUP BY Modele_de_produit.code_EAN13,Ticket_de_caisse.Identifiant_Magasin
 
ORDER BY Ticket_de_caisse.Identifiant_Magasin;


#---Final---
# Etablissez par magasin, pour le mois précédent et pour chaque type de produit le
# rapprochement entre le prévisionnel et le réalisé (magasin, type de produit, quantité
# prévisionnelle, quantité vendue)

SELECT 
	Previsionnel.Identifiant_Magasin,
    Previsionnel.Type_de_produit,
    Previsionnel.Quantite AS Quantite_Prevue,
    Vente.Quantite_Vendue

FROM 	(
		SELECT
			Prevision_de_vente.Identifiant_Magasin,
			Modele_de_produit.Type_de_produit,
			Prevision_de_vente.Quantite
			
		FROM Prevision_de_vente
			INNER 
				JOIN Modele_de_produit 	ON (Modele_de_produit.code_EAN13 = Prevision_de_vente.code_EAN13)

		WHERE Prevision_de_vente.Date_prevision 
			LIKE '__-02'
			
		ORDER BY  Prevision_de_vente.Identifiant_Magasin
        ) AS Previsionnel
	INNER
		JOIN 
			(
            SELECT 
				COUNT(quantite_modele_par_ticket.Un_Produit_Vendu) AS Quantite_vendue,
				Modele_de_produit.Type_de_produit,
				Ticket_de_caisse.Identifiant_Magasin
				
			FROM 	(
					SELECT Ligne_de_ticket_de_caisse.Identifiant  				AS Un_Produit_Vendu,
						Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse 	AS Identifiant_Ticket,
						Ligne_de_ticket_de_caisse.code_EAN13                   	AS EAN13
						
					FROM Ligne_de_ticket_de_caisse
					) AS quantite_modele_par_ticket
				INNER 
					JOIN Ticket_de_caisse 	ON (Ticket_de_caisse.Identifiant = quantite_modele_par_ticket.Identifiant_Ticket)
					JOIN Modele_de_produit 	ON (Modele_de_produit.code_EAN13 = quantite_modele_par_ticket.EAN13)
			 
			WHERE Ticket_de_caisse.Date_de_vente LIKE '__-02' 

			GROUP BY Modele_de_produit.code_EAN13,Ticket_de_caisse.Identifiant_Magasin
			 
			ORDER BY Ticket_de_caisse.Identifiant_Magasin
            ) AS Vente
		ON (Previsionnel.Identifiant_Magasin = Vente.Identifiant_Magasin)
        
	
    ORDER BY Identifiant_Magasin;




﻿#__________________________________________________________________________________________________________________________________________________________________
# === 3 ou 5 ou 7 ===


# filtre par date et regroupement par magasin
select Ticket_de_caisse.Identifiant, Ticket_de_caisse.Date_de_vente 
from Ticket_de_caisse
where Date_de_vente like '%2016%'
group by Ticket_de_caisse.Identifiant_Magasin;


# produits par type
select Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse
from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13);


# comptage des produits par type
select * from (
select Modele_de_produit.Type_de_produit, COUNT(Ligne_de_ticket_de_caisse.Identifiant) as Quantité_de_produits_vendus
from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
group by Modele_de_produit.Type_de_produit
)as Nombre_produit_par_type;


# comptage des produits par type par magasin par année
select * from(
select Ticket_de_caisse.Date_de_vente, Ticket_de_caisse.Identifiant_Magasin, 
		Nombre_produit_par_type.Type_de_produit, COUNT(Nombre_produit_par_type.Identifiant) as Quantité_de_produits_vendus 
		from (
			select Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse
			from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
			group by Modele_de_produit.Type_de_produit
		)as Nombre_produit_par_type
		inner join Ticket_de_caisse
		on Nombre_produit_par_type.Identifiant_Ticket_de_caisse = Ticket_de_caisse.Identifiant
		where Ticket_de_caisse.Date_de_vente like '%2016%'
		group by Ticket_de_caisse.Identifiant_Magasin
) as Nombre_produit_par_type_par_magasin_par_annee;


# quantité de produits par achat par type de produit
select * from (
select Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse, count(Ligne_de_ticket_de_caisse.Identifiant) as Nombre_de_produits_achétés_par_achat
from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
group by Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse
)as Quantite_type_produit_par_achat;


# quantité de produits par achat par type de produit par magasin par année
select * from (
select Ticket_de_caisse.Date_de_vente, Ticket_de_caisse.Identifiant_Magasin,
		Quantite_moyenne_type_produit_par_achat.Type_de_produit, Quantite_moyenne_type_produit_par_achat.Identifiant_Ticket_de_caisse,
		Quantite_moyenne_type_produit_par_achat.Nombre_de_produits_achétés_par_achat
from (select Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse, count(Ligne_de_ticket_de_caisse.Identifiant) as Nombre_de_produits_achétés_par_achat
			from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
			group by Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse
		)as Quantite_moyenne_type_produit_par_achat
		inner join Ticket_de_caisse
		on Quantite_moyenne_type_produit_par_achat.Identifiant_Ticket_de_caisse = Ticket_de_caisse.Identifiant
		where Ticket_de_caisse.Date_de_vente like '%2016%'
		group by Ticket_de_caisse.Identifiant_Magasin
) as Quantite_type_produit_par_achat_par_magasin_par_annee;


# quantité de produits par achat par type de produit par magasin par année
select * from(
select Quantite_type_produit_par_achat_par_magasin_par_annee.Date_de_vente, Quantite_type_produit_par_achat_par_magasin_par_annee.Identifiant_Magasin,
			Quantite_type_produit_par_achat_par_magasin_par_annee.Type_de_produit, 
			avg(Quantite_type_produit_par_achat_par_magasin_par_annee.Nombre_de_produits_achétés_par_achat) as Moyenne_produits_achetés_par_achat
from (
	select Ticket_de_caisse.Date_de_vente, Ticket_de_caisse.Identifiant_Magasin,
			Quantite_moyenne_type_produit_par_achat.Type_de_produit, Quantite_moyenne_type_produit_par_achat.Identifiant_Ticket_de_caisse,
			Quantite_moyenne_type_produit_par_achat.Nombre_de_produits_achétés_par_achat
	from (select Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse, count(Ligne_de_ticket_de_caisse.Identifiant) as Nombre_de_produits_achétés_par_achat
				from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
				group by Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse
			)as Quantite_moyenne_type_produit_par_achat
			inner join Ticket_de_caisse
			on Quantite_moyenne_type_produit_par_achat.Identifiant_Ticket_de_caisse = Ticket_de_caisse.Identifiant
			where Ticket_de_caisse.Date_de_vente like '%2016%'
			group by Ticket_de_caisse.Identifiant_Magasin
	) as Quantite_type_produit_par_achat_par_magasin_par_annee
group by Quantite_type_produit_par_achat_par_magasin_par_annee.Type_de_produit
) as Quantite_moyenne_type_produit_par_achat_par_magasin_par_annee;


# requête finale : liste des produits achetés par magasin (magasin, type produit, quantité, quantité moyenne par achat) pour une période donnée (année)
select Nombre_produit_par_type_par_magasin_par_annee.Date_de_vente, Nombre_produit_par_type_par_magasin_par_annee.Identifiant_Magasin, 
	Nombre_produit_par_type_par_magasin_par_annee.Type_de_produit, Nombre_produit_par_type_par_magasin_par_annee.Quantité_de_produits_vendus,
	Quantite_moyenne_type_produit_par_achat_par_magasin_par_annee.Moyenne_produits_achetés_par_achat
		
from (
	select Ticket_de_caisse.Date_de_vente, Ticket_de_caisse.Identifiant_Magasin, 
			Nombre_produit_par_type.Type_de_produit, COUNT(Nombre_produit_par_type.Identifiant) as Quantité_de_produits_vendus 
			from (
				select Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse
				from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
				group by Modele_de_produit.Type_de_produit
			)as Nombre_produit_par_type
			inner join Ticket_de_caisse
			on Nombre_produit_par_type.Identifiant_Ticket_de_caisse = Ticket_de_caisse.Identifiant
			where Ticket_de_caisse.Date_de_vente like '%2016%'
			group by Ticket_de_caisse.Identifiant_Magasin
	) as Nombre_produit_par_type_par_magasin_par_annee
inner join (
	select Quantite_type_produit_par_achat_par_magasin_par_annee.Date_de_vente, Quantite_type_produit_par_achat_par_magasin_par_annee.Identifiant_Magasin,
				Quantite_type_produit_par_achat_par_magasin_par_annee.Type_de_produit, 
				avg(Quantite_type_produit_par_achat_par_magasin_par_annee.Nombre_de_produits_achétés_par_achat) as Moyenne_produits_achetés_par_achat
	from (
		select Ticket_de_caisse.Date_de_vente, Ticket_de_caisse.Identifiant_Magasin,
				Quantite_moyenne_type_produit_par_achat.Type_de_produit, Quantite_moyenne_type_produit_par_achat.Identifiant_Ticket_de_caisse,
				Quantite_moyenne_type_produit_par_achat.Nombre_de_produits_achétés_par_achat
		from (select Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse, count(Ligne_de_ticket_de_caisse.Identifiant) as Nombre_de_produits_achétés_par_achat
					from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
					group by Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse
				)as Quantite_moyenne_type_produit_par_achat
				inner join Ticket_de_caisse
				on Quantite_moyenne_type_produit_par_achat.Identifiant_Ticket_de_caisse = Ticket_de_caisse.Identifiant
				where Ticket_de_caisse.Date_de_vente like '%2016%'
				group by Ticket_de_caisse.Identifiant_Magasin
		) as Quantite_type_produit_par_achat_par_magasin_par_annee
	group by Quantite_type_produit_par_achat_par_magasin_par_annee.Type_de_produit
	) as Quantite_moyenne_type_produit_par_achat_par_magasin_par_annee
on (Nombre_produit_par_type_par_magasin_par_annee.Date_de_vente = Quantite_moyenne_type_produit_par_achat_par_magasin_par_annee.Date_de_vente) 
	and (Nombre_produit_par_type_par_magasin_par_annee.Identifiant_Magasin = Quantite_moyenne_type_produit_par_achat_par_magasin_par_annee.Identifiant_Magasin) 
	and (Nombre_produit_par_type_par_magasin_par_annee.Type_de_produit = Quantite_moyenne_type_produit_par_achat_par_magasin_par_annee.Type_de_produit) 

order by Nombre_produit_par_type_par_magasin_par_annee.Identifiant_Magasin, Nombre_produit_par_type_par_magasin_par_annee.Type_de_produit, 
		Nombre_produit_par_type_par_magasin_par_annee.Date_de_vente;



# === 5

# requête finale : liste des produits achetés par client (client, type produit, quantité, quantité moyenne par achat) pour une période donnée (année)
select Nombre_produit_par_type_par_client_par_annee.Date_de_vente, Nombre_produit_par_type_par_client_par_annee.Identifiant_Client, 
	Nombre_produit_par_type_par_client_par_annee.Type_de_produit, Nombre_produit_par_type_par_client_par_annee.Quantité_de_produits_vendus,
	Quantite_moyenne_type_produit_par_achat_par_client_par_annee.Moyenne_produits_achetés_par_achat
		
from (
	select Ticket_de_caisse.Date_de_vente, Ticket_de_caisse.Identifiant_Client, 
			Nombre_produit_par_type.Type_de_produit, COUNT(Nombre_produit_par_type.Identifiant) as Quantité_de_produits_vendus 
			from (
				select Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse
				from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
				group by Modele_de_produit.Type_de_produit
			)as Nombre_produit_par_type
			inner join Ticket_de_caisse
			on Nombre_produit_par_type.Identifiant_Ticket_de_caisse = Ticket_de_caisse.Identifiant
			where Ticket_de_caisse.Date_de_vente like '%2016%'
			group by Ticket_de_caisse.Identifiant_Client
	) as Nombre_produit_par_type_par_client_par_annee
inner join (
	select Quantite_type_produit_par_achat_par_client_par_annee.Date_de_vente, Quantite_type_produit_par_achat_par_client_par_annee.Identifiant_Client,
				Quantite_type_produit_par_achat_par_client_par_annee.Type_de_produit, 
				avg(Quantite_type_produit_par_achat_par_client_par_annee.Nombre_de_produits_achétés_par_achat) as Moyenne_produits_achetés_par_achat
	from (
		select Ticket_de_caisse.Date_de_vente, Ticket_de_caisse.Identifiant_Client,
				Quantite_moyenne_type_produit_par_achat.Type_de_produit, Quantite_moyenne_type_produit_par_achat.Identifiant_Ticket_de_caisse,
				Quantite_moyenne_type_produit_par_achat.Nombre_de_produits_achétés_par_achat
		from (select Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse, count(Ligne_de_ticket_de_caisse.Identifiant) as Nombre_de_produits_achétés_par_achat
					from Ligne_de_ticket_de_caisse inner join Modele_de_produit on (Ligne_de_ticket_de_caisse.code_EAN13 = Modele_de_produit.code_EAN13)
					group by Modele_de_produit.Type_de_produit, Ligne_de_ticket_de_caisse.Identifiant_Ticket_de_caisse
				)as Quantite_moyenne_type_produit_par_achat
				inner join Ticket_de_caisse
				on Quantite_moyenne_type_produit_par_achat.Identifiant_Ticket_de_caisse = Ticket_de_caisse.Identifiant
				where Ticket_de_caisse.Date_de_vente like '%2016%'
				group by Ticket_de_caisse.Identifiant_Client
		) as Quantite_type_produit_par_achat_par_client_par_annee
	group by Quantite_type_produit_par_achat_par_client_par_annee.Type_de_produit
	) as Quantite_moyenne_type_produit_par_achat_par_client_par_annee
on (Nombre_produit_par_type_par_client_par_annee.Date_de_vente = Quantite_moyenne_type_produit_par_achat_par_client_par_annee.Date_de_vente) 
	and (Nombre_produit_par_type_par_client_par_annee.Identifiant_Client = Quantite_moyenne_type_produit_par_achat_par_client_par_annee.Identifiant_Client) 
	and (Nombre_produit_par_type_par_client_par_annee.Type_de_produit = Quantite_moyenne_type_produit_par_achat_par_client_par_annee.Type_de_produit) 

order by Nombre_produit_par_type_par_client_par_annee.Identifiant_Client, Nombre_produit_par_type_par_client_par_annee.Type_de_produit, 
		Nombre_produit_par_type_par_client_par_annee.Date_de_vente;

