# authors : Clement Brohon & Clement Andry

# === 5 ===

SELECT Ticket_de_caisse.Identifiant_Magasin, Modele_de_produit.Libelle, COUNT(Produit_unique.Identifiant) as quantite_de_produit_unique
FROM Ticket_de_caisse JOIN Ligne_de_ticket_de_caisse 
ON Ligne_de_ticket_de_caisse.Identifiant_ticket_de_caisse = Ticket_de_caisse.Identifiant 

JOIN Produit_unique 
ON Ligne_de_ticket_de_caisse.Identifiant_produit_unique = Produit_unique.Identifiant 

JOIN Modele_de_produit 
ON Produit_unique.code_EAN13 = Modele_de_produit.code_EAN13 

WHERE Ticket_de_caisse.Date_de_vente < '2016-01-01' AND Ticket_de_caisse.Date_de_vente > '2016-01-31' 

GROUP BY Ticket_de_caisse.Identifiant_Magasin, Modele_de_produit.code_EAN13 
ORDER BY quantite_de_produit_unique
LIMIT 10;


# === 6 ===

SELECT Modele_de_produit.Libelle, 
	Modele_de_produit.Points_de_fidelite_attribues * COUNT( Produit_unique.Identifiant) as points_fidelite_distribues, 
	COUNT( Produit_unique.Identifiant) * Prix_de_vente.Prix as chiffre_d_affaire
FROM Modele_de_produit

JOIN Produit_unique
ON Produit_unique.code_EAN13 = Modele_de_produit.code_EAN13

JOIN Ligne_de_ticket_de_caisse
ON Ligne_de_ticket_de_caisse.Identifiant_produit_unique = Produit_unique.Identifiant

JOIN Ticket_de_caisse
ON Ligne_de_ticket_de_caisse.Identifiant_ticket_de_caisse = Ticket_de_caisse.Identifiant

JOIN Prix_de_vente
ON Prix_de_vente.code_EAN13 = Modele_de_produit.code_EAN13

WHERE Prix_de_vente.Date_debut_validite < Ticket_de_caisse.Date_de_vente AND Prix_de_vente.Date_fin_validite > Ticket_de_caisse.Date_de_vente
AND Ticket_de_caisse.Date_de_vente LIKE '__-02%'#BETWEEN LAST_DAY(TRUNC(ADD_MONTHS(SYSDATE, -2))) + 1 AND LAST_DAY(TRUNC(ADD_MONTHS(SYSDATE, -1))

GROUP BY Modele_de_produit.Libelle
ORDER BY Modele_de_produit.Libelle;
