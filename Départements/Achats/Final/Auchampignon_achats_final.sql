#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Modèle de produit
#------------------------------------------------------------

CREATE TABLE Modele_de_produit(
        code_EAN13                   Varchar (13) NOT NULL ,
        Libelle                      Varchar (25) NOT NULL ,
        Type_de_produit              Varchar (25) ,
        Points_de_fidelite_attribues Int ,
        Poids                        Float ,
        PRIMARY KEY (code_EAN13 )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Fournisseur
#------------------------------------------------------------

CREATE TABLE Fournisseur(
        SIRET          Varchar (25) NOT NULL ,
        Raison_sociale Varchar (25) ,
        PRIMARY KEY (SIRET )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Gamme
#------------------------------------------------------------

CREATE TABLE Gamme(
        Identifiant int (11) Auto_increment  NOT NULL ,
        Libelle     Varchar (25) NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Commande
#------------------------------------------------------------

CREATE TABLE Commande(
        Identifiant                   int (11) Auto_increment  NOT NULL ,
        Date_de_commande              Date ,
        Date_de_livraison_imperative  Date ,
        Date_de_livraison_effective   Date ,
        Identifiant_Site_de_livraison Int NOT NULL ,
        SIRET                         Varchar (25) NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Site de livraison
#------------------------------------------------------------

CREATE TABLE Site_de_livraison(
        Identifiant                  int (11) Auto_increment  NOT NULL ,
        Type_de_site                 Varchar (25) ,
        Identifiant_Commande         Int NOT NULL ,
        Identifiant_Zone_de_stockage Int ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Tailles
#------------------------------------------------------------

CREATE TABLE Tailles(
        Identifiant int (11) Auto_increment  NOT NULL ,
        Libelle     Varchar (25) ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Coloris
#------------------------------------------------------------

CREATE TABLE Coloris(
        Identifiant int (11) Auto_increment  NOT NULL ,
        Libelle     Varchar (25) NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Produit unique
#------------------------------------------------------------

CREATE TABLE Produit_unique(
        Identifiant                             int (11) Auto_increment  NOT NULL ,
        Date_de_peremption                      Date ,
        code_EAN13                              Varchar (13) NOT NULL ,
        Numero_de_ligne_de_commande             Int ,
        Identifiant_Commande                    Int NOT NULL ,
        Identifiant_Combinaison_tailles_coloris Int NOT NULL ,
        PRIMARY KEY (Identifiant ,code_EAN13 )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Combinaison tailles coloris
#------------------------------------------------------------

CREATE TABLE Combinaison_tailles_coloris(
        Identifiant int (11) Auto_increment  NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Tarif HT
#------------------------------------------------------------

CREATE TABLE Tarif_HT(
        Identifiant                        int (11) Auto_increment  NOT NULL ,
        Tarif                              DECIMAL (15,3)  NOT NULL ,
        Date_de_debut_de_validite_du_tarif Date ,
        Date_de_fin_de_validite_du_tarif   Date ,
        Quantite_du_produit                Int NOT NULL ,
        SIRET                              Varchar (25) NOT NULL ,
        code_EAN13                         Varchar (13) ,
        PRIMARY KEY (Identifiant ,SIRET )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Ligne de commande
#------------------------------------------------------------

CREATE TABLE Ligne_de_commande(
        Numero_de_ligne_de_commande    Int NOT NULL ,
        Quantite_de_produits_commandes Int ,
        Identifiant                    Int NOT NULL ,
        code_EAN13                     Varchar (13) ,
        PRIMARY KEY (Numero_de_ligne_de_commande ,Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Ligne de modèle de produit
#------------------------------------------------------------

CREATE TABLE Ligne_de_modele_de_produit(
        Identifiant int (11) Auto_increment  NOT NULL ,
        Libelle     Varchar (25) NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Adresse
#------------------------------------------------------------

CREATE TABLE Adresse(
        Identifiant int (11) Auto_increment  NOT NULL ,
        Numero      Varchar (25) ,
        Voie        Varchar (25) ,
        Code_postal Varchar (25) ,
        Ville       Varchar (25) ,
        Pays        Varchar (25) ,
        Remarques   Longtext ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Zone de stockage
#------------------------------------------------------------

CREATE TABLE Zone_de_stockage(
        Identifiant                        int (11) Auto_increment  NOT NULL ,
        Type_de_zone_de_stockage           Varchar (25) ,
        Capacite_stockage_ambiant_max      Int ,
        Capacite_stockage_refrigere_max    Int ,
        Capacite_stockage_congele_max      Int ,
        Capacite_stockage_ambiant_actuel   Int ,
        Capacite_stockage_refrigere_actuel Int ,
        Capacite_stockage_congele_actuel   Int ,
        Identifiant_Site_de_livraison      Int NOT NULL ,
        Identifiant_Adresse                Int NOT NULL ,
        PRIMARY KEY (Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Regrouper
#------------------------------------------------------------

CREATE TABLE Regrouper(
        code_EAN13  Varchar (13) NOT NULL ,
        Identifiant Int NOT NULL ,
        PRIMARY KEY (code_EAN13 ,Identifiant )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Tailles possibles
#------------------------------------------------------------

CREATE TABLE Tailles_possibles(
        Identifiant                             Int NOT NULL ,
        Identifiant_Combinaison_tailles_coloris Int NOT NULL ,
        PRIMARY KEY (Identifiant ,Identifiant_Combinaison_tailles_coloris )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Coloris possibles
#------------------------------------------------------------

CREATE TABLE Coloris_possibles(
        Identifiant                             Int NOT NULL ,
        Identifiant_Combinaison_tailles_coloris Int NOT NULL ,
        PRIMARY KEY (Identifiant ,Identifiant_Combinaison_tailles_coloris )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Fournir
#------------------------------------------------------------

CREATE TABLE Fournir(
        Code_fournisseur Varchar (25) NOT NULL ,
        code_EAN13       Varchar (13) NOT NULL ,
        SIRET            Varchar (25) NOT NULL ,
        PRIMARY KEY (code_EAN13 ,SIRET )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Combinaisons disponibles
#------------------------------------------------------------

CREATE TABLE Combinaisons_disponibles(
        Identifiant Int NOT NULL ,
        code_EAN13  Varchar (13) NOT NULL ,
        PRIMARY KEY (Identifiant ,code_EAN13 )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Appartenir gamme
#------------------------------------------------------------

CREATE TABLE Appartenir_gamme(
        Identifiant                            Int NOT NULL ,
        Identifiant_Ligne_de_modele_de_produit Int NOT NULL ,
        PRIMARY KEY (Identifiant ,Identifiant_Ligne_de_modele_de_produit )
)ENGINE=InnoDB;

ALTER TABLE Commande ADD CONSTRAINT FK_Commande_Identifiant_Site_de_livraison FOREIGN KEY (Identifiant_Site_de_livraison) REFERENCES Site_de_livraison(Identifiant);
ALTER TABLE Commande ADD CONSTRAINT FK_Commande_SIRET FOREIGN KEY (SIRET) REFERENCES Fournisseur(SIRET);
ALTER TABLE Site_de_livraison ADD CONSTRAINT FK_Site_de_livraison_Identifiant_Commande FOREIGN KEY (Identifiant_Commande) REFERENCES Commande(Identifiant);
ALTER TABLE Site_de_livraison ADD CONSTRAINT FK_Site_de_livraison_Identifiant_Zone_de_stockage FOREIGN KEY (Identifiant_Zone_de_stockage) REFERENCES Zone_de_stockage(Identifiant);
ALTER TABLE Produit_unique ADD CONSTRAINT FK_Produit_unique_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Produit_unique ADD CONSTRAINT FK_Produit_unique_Numero_de_ligne_de_commande FOREIGN KEY (Numero_de_ligne_de_commande) REFERENCES Ligne_de_commande(Numero_de_ligne_de_commande);
ALTER TABLE Produit_unique ADD CONSTRAINT FK_Produit_unique_Identifiant_Commande FOREIGN KEY (Identifiant_Commande) REFERENCES Commande(Identifiant);
ALTER TABLE Produit_unique ADD CONSTRAINT FK_Produit_unique_Identifiant_Combinaison_tailles_coloris FOREIGN KEY (Identifiant_Combinaison_tailles_coloris) REFERENCES Combinaison_tailles_coloris(Identifiant);
ALTER TABLE Tarif_HT ADD CONSTRAINT FK_Tarif_HT_SIRET FOREIGN KEY (SIRET) REFERENCES Fournisseur(SIRET);
ALTER TABLE Tarif_HT ADD CONSTRAINT FK_Tarif_HT_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Ligne_de_commande ADD CONSTRAINT FK_Ligne_de_commande_Identifiant FOREIGN KEY (Identifiant) REFERENCES Commande(Identifiant);
ALTER TABLE Ligne_de_commande ADD CONSTRAINT FK_Ligne_de_commande_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Zone_de_stockage ADD CONSTRAINT FK_Zone_de_stockage_Identifiant_Site_de_livraison FOREIGN KEY (Identifiant_Site_de_livraison) REFERENCES Site_de_livraison(Identifiant);
ALTER TABLE Zone_de_stockage ADD CONSTRAINT FK_Zone_de_stockage_Identifiant_Adresse FOREIGN KEY (Identifiant_Adresse) REFERENCES Adresse(Identifiant);
ALTER TABLE Regrouper ADD CONSTRAINT FK_Regrouper_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Regrouper ADD CONSTRAINT FK_Regrouper_Identifiant FOREIGN KEY (Identifiant) REFERENCES Ligne_de_modele_de_produit(Identifiant);
ALTER TABLE Tailles_possibles ADD CONSTRAINT FK_Tailles_possibles_Identifiant FOREIGN KEY (Identifiant) REFERENCES Tailles(Identifiant);
ALTER TABLE Tailles_possibles ADD CONSTRAINT FK_Tailles_possibles_Identifiant_Combinaison_tailles_coloris FOREIGN KEY (Identifiant_Combinaison_tailles_coloris) REFERENCES Combinaison_tailles_coloris(Identifiant);
ALTER TABLE Coloris_possibles ADD CONSTRAINT FK_Coloris_possibles_Identifiant FOREIGN KEY (Identifiant) REFERENCES Coloris(Identifiant);
ALTER TABLE Coloris_possibles ADD CONSTRAINT FK_Coloris_possibles_Identifiant_Combinaison_tailles_coloris FOREIGN KEY (Identifiant_Combinaison_tailles_coloris) REFERENCES Combinaison_tailles_coloris(Identifiant);
ALTER TABLE Fournir ADD CONSTRAINT FK_Fournir_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Fournir ADD CONSTRAINT FK_Fournir_SIRET FOREIGN KEY (SIRET) REFERENCES Fournisseur(SIRET);
ALTER TABLE Combinaisons_disponibles ADD CONSTRAINT FK_Combinaisons_disponibles_Identifiant FOREIGN KEY (Identifiant) REFERENCES Combinaison_tailles_coloris(Identifiant);
ALTER TABLE Combinaisons_disponibles ADD CONSTRAINT FK_Combinaisons_disponibles_code_EAN13 FOREIGN KEY (code_EAN13) REFERENCES Modele_de_produit(code_EAN13);
ALTER TABLE Appartenir_gamme ADD CONSTRAINT FK_Appartenir_gamme_Identifiant FOREIGN KEY (Identifiant) REFERENCES Gamme(Identifiant);
ALTER TABLE Appartenir_gamme ADD CONSTRAINT FK_Appartenir_gamme_Identifiant_Ligne_de_modele_de_produit FOREIGN KEY (Identifiant_Ligne_de_modele_de_produit) REFERENCES Ligne_de_modele_de_produit(Identifiant);

